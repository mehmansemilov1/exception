package Task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean validInput = false;

        while (!validInput) {
            try {
                System.out.print("integer daxil edin: ");
                int num = scanner.nextInt();
                System.out.println("daxil edilen reqemin " + num + " kvadrati " + (num * num));
                validInput = true;
            } catch (InputMismatchException e) {
                System.out.println("Yalnış daxiletme.Tam eded daxil edin.");
                scanner.nextLine();
            }
        }
        scanner.close();
    }
}

