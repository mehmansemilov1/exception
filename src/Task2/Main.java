package Task2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.print("1 ile 100 arasinda reqem daxil edin: ");
            int num = scanner.nextInt();
            if (num < 1 || num > 100) {
                throw new InvalidInputException("Nomre 1 ilə 100 arasında olmalıdır.");
            }
            System.out.println("Daxil edilen reqem: " + num);
        } catch (InputMismatchException e) {
            System.out.println("Yalnış reqem daxil etdiz. Tam ədəd daxil edin.");
        } catch (InvalidInputException e) {
            System.out.println(e.getMessage());
        } finally {
            scanner.close();
        }

    }
}
